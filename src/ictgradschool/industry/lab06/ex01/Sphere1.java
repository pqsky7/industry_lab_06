package ictgradschool.industry.lab06.ex01;

import java.util.ArrayList;

/**
 * Should represent a Sphere.
 * <p>
 * TODO Implement this class.e
 */

public class Sphere1 {

    private void start() {
        int count;
        ArrayList<double[]> coordinate = new ArrayList<>();
        double[] radius;
        double[] areas;
        double[] volumes;
        count = getCount();
        radius = getRadius(count);
        coordinate = getCoordinate(count);
        areas = getArea(count, radius);
        volumes = getVolume(count, radius);
        printResults(count, radius, coordinate, areas, volumes);
    }

    private int getCount() {
        return (int) (Math.random() * 10 + 1);
    }

    private double generateRadius() {
        return Math.random() * 100 + 1;
    }

    private double[] generateCoordinates() {
        double[] coordinates = new double[3];
        for (int i = 0; i < 3; i++) {
            coordinates[i] = Math.round((Math.random() * 200 - 100) * 100.0) / 100.0;
        }
        return coordinates;
    }

    private ArrayList getCoordinate(int count) {
        ArrayList coordinates = new ArrayList();
        for (int i = 0; i < count; i++) {
            coordinates.add(generateCoordinates());
        }
        return coordinates;
    }

    private double[] getRadius(int count) {
        double[] radius = new double[count];
        for (int i = 0; i < count; i++) {
            radius[i] = generateRadius();
        }
        return radius;
    }

    private double[] getArea(int count, double[] radius) {
        double[] area = new double[count];
        for (int i = 0; i < count; i++) {
            area[i] = 4 * Math.PI * Math.pow(radius[i], 2);
        }
        return area;
    }

    private double[] getVolume(int count, double[] radius) {
        double[] volume = new double[count];
        for (int i = 0; i < count; i++) {
            volume[i] = 4 / 3 * Math.PI * Math.pow(radius[i], 3);
        }
        return volume;
    }

    private void printResults(int count, double[] radius, ArrayList<double[]> coordinates, double[] areas, double[] volume) {
        System.out.println("Welcome to lab06 Exercise One - Simple Sphere class");
        System.out.println("The program has generate " + count + " spheres");
        double[] cod = new double[3];
        for (int i = 0; i < count; i++) {
            cod = coordinates.get(i);
            System.out.println("The No." + (i + 1) + " sphere: radius: " + Math.round(radius[i]) + ", coordinate: (" + cod[0] + " ," + cod[1] + ", " + cod[2] + "), area: " + Math.round(areas[i]) + ", volume: " + Math.round(volume[i]));
        }
    }


    public static void main(String[] args) {
        Sphere1 Q = new Sphere1();
        Q.start();
    }

}
